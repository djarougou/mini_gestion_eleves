package application;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnexionBD {
	private static Connection connexion;
	private static Statement statement = null;

	public static boolean creerConnexion() {
		try {
			connexion = DriverManager.getConnection("jdbc:mysql://localhost:3308/gestion_eleves?useSSL=false", "root",
					"");

			statement = connexion.createStatement();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public static boolean fermerConnexion() {
		try {
			connexion.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public static Statement getStatement() {
		return ConnexionBD.statement;
	}

	public static Connection getConnexion() {
		creerConnexion();
		return ConnexionBD.connexion;
	}
	public static void main(String[] args) {
		System.out.println(creerConnexion());
	}

}
