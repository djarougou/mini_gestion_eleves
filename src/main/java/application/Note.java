package application;

import javax.persistence.Id;

 
public class Note {
	
	@Id
	private int idNote;
	private String matiere;
	private double note;
	private String mention;
	public String getMatiere() {
		return matiere;
	}
	public Note(String matiere, double note, String mention) {
	 
		this.matiere = matiere;
		this.note = note;
		this.mention = mention;
	}
	 
	public Note(int idNote, String matiere, double note, String mention) {
		super();
		this.idNote = idNote;
		this.matiere = matiere;
		this.note = note;
		this.mention = mention;
	}
	public void setMatiere(String matiere) {
		this.matiere = matiere;
	}
	public double getNote() {
		return note;
	}
	public void setNote(double note) {
		this.note = note;
	}
	public String getMention() {
		return mention;
	}
	public void setMention(String mention) {
		this.mention = mention;
	}
	public Note() {
		this(0,"",0.0,"");
	}
	

}
