package application;

import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Optional;
import java.util.ResourceBundle;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;

import com.jfoenix.controls.JFXTextField;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

public class NoteController implements Initializable {
	 @FXML
	    private StackPane contentPane;
	@FXML
	private TableView<Note> noteTable;

	@FXML
	private TableColumn<Note, String> matiereCol;

	@FXML
	private TableColumn<Note, Double> noteCol;

	@FXML
	private TableColumn<Note, String> appreciCol;

	@FXML
	private Label totalNote;

	@FXML
	private Label totalMatier;

	@FXML
	private Label moyenne;

	@FXML
	private Label totalNote1;
	@FXML
	private Label nomLabel;

	@FXML
	private JFXTextField txtNote;
	@FXML
    private JFXTextField txtAppre;
	@FXML public ObservableList<Note> data= FXCollections.observableArrayList();

	@FXML
	void chargerDetails(MouseEvent event) {
		if(event.getClickCount()==2) {
			Note no =noteTable.getSelectionModel().getSelectedItem();
			txtNote.setText(no.getNote()+"");
			txtAppre.setText(no.getMention());
			int i = noteTable.getSelectionModel().getSelectedIndex();
			System.out.println(i);
			
		}
	}
	  @FXML
	    void imprimer(ActionEvent event) throws IOException {
		  
		  String chemin="";
		  
		  // chemin
		  DirectoryChooser directoryChooser = new DirectoryChooser();
			File dir =directoryChooser.showDialog(new Stage());
			if(dir!=null) {
				chemin+=dir.getAbsolutePath();
			}
		  System.out.println(chemin);
		  
		  HSSFWorkbook workbook = new HSSFWorkbook();
			HSSFSheet sheet = workbook.createSheet("Notes");
			ObservableList<Note> listEleves = data;

			int rownum = 0;
			Cell cell;
			Row row;
			//
			HSSFCellStyle style = createStyleForTitle(workbook);

			row = sheet.createRow(rownum);
			
			// remplissage de nom 
			 cell = row.createCell(1,CellType.STRING);
			 cell.setCellValue(nomLabel.getText());
			 cell.setCellStyle(style);
			
			
			
			//entete
			 rownum++;
			 row=sheet.createRow(rownum);

			// Matiere
			cell = row.createCell(1, CellType.STRING);
			cell.setCellValue("Matiere");
			cell.setCellStyle(style);
			//note
			cell = row.createCell(2, CellType.NUMERIC);
			cell.setCellValue("Note");
			cell.setCellStyle(style);
			// Observation
			cell = row.createCell(3, CellType.STRING);
			cell.setCellValue("Appreciation");
			cell.setCellStyle(style);
			
			 

			// Data
			for (Note note : data) {
				rownum++;
				row = sheet.createRow(rownum);
                 // cellule matiere
				cell = row.createCell(1, CellType.STRING);
				cell.setCellValue(note.getMatiere());
				cell.setCellStyle(style);
				
				// cellule note (B)
				cell = row.createCell(2, CellType.NUMERIC);
				cell.setCellValue(note.getNote());
				// cellule Appreciation  (C)
				cell = row.createCell(3, CellType.STRING);
				cell.setCellValue(note.getMention());
				// moyenne eleve (D)
			 
			}
			
			// remplire les totaux
			rownum++;
			row = sheet.createRow(rownum);
		    cell=row.createCell(0,CellType.STRING);
		    cell.setCellValue("Totaux");
		    cell.setCellStyle(style);
		    
		    cell= row.createCell(1, CellType.NUMERIC);
		    
		    cell.setCellValue(Double.parseDouble(totalMatier.getText()));
		    
		    
		    cell=row.createCell(2, CellType.NUMERIC);
		    cell.setCellValue(Double.parseDouble(totalNote.getText()));
		    
		 
		    // la ligne de moyenne
		    rownum++;
		    row = sheet.createRow(rownum);
		    
		    cell = row.createCell(1, CellType.STRING);
		    cell.setCellValue("Moyenne:");
		    cell.setCellStyle(style);
		    
		    cell = row.createCell(2, CellType.NUMERIC);
		    cell.setCellValue(Double.parseDouble(moyenne.getText()));
		    
		    String nomComple=chemin+"\\"+"Notes.xls";
		    
		    File file = new File(nomComple);
			file.getParentFile().mkdirs();

			FileOutputStream outFile = new FileOutputStream(file);
			workbook.write(outFile);
			System.out.println("Created file: " + file.getAbsolutePath());
			
			alerter(file);
			
	 


	    }
	  
     public void alerter(File f) {
		   
			  
		      Alert alert = new Alert(AlertType.CONFIRMATION);
		      alert.setTitle("Ouverture");
		      alert.setHeaderText("VoulezVous ouvrir le fichier maintenant");
	 
		      Optional<ButtonType> option = alert.showAndWait();
		 
		       if (option.get() == ButtonType.OK) {
		         try {
					Desktop.getDesktop().open(f);
				} catch (IOException e) {
					 
					e.printStackTrace();
				}
		      } else if (option.get() == ButtonType.CANCEL) {
		          
		      }  
		   
	  }
	  public HSSFCellStyle createStyleForTitle(HSSFWorkbook workbook) {
			HSSFFont font = workbook.createFont();
			font.setBold(true);
			HSSFCellStyle style = workbook.createCellStyle();
			style.setFont(font);
			return style;
		}
	  	  
	  

	@FXML
	void mettreAjour(ActionEvent event) {
	    Note note = noteTable.getSelectionModel().getSelectedItem();
	    try {

			Connection con = ConnexionBD.getConnexion();
			String sql = "UPDATE note set note=?,appreciation=? where matiere like ? and idEl=? ";
			PreparedStatement stm = con.prepareStatement(sql);
			stm.setDouble(1, Double.parseDouble( txtNote.getText()));
			stm.setString(2, txtAppre.getText());
			stm.setString(3, note.getMatiere());
			stm.setInt(4, EleveController.selectedEleve.getId());
	 
			int i = stm.executeUpdate();

			if (i > 0) {
				System.out.println("resusiiiie");
				afficher();
				clear();
			    calculSomme();
			}
			con.close();
			

		} catch (Exception e) {
			e.printStackTrace();
		}
     
	}
	
	public void clear() {
		txtAppre.clear();
		txtNote.clear();
	}
	  @FXML
	    void deconnexion(ActionEvent event) {
		  FXMLLoader loader = new FXMLLoader(this.getClass().getResource("Login.fxml"));
  	    try {
  	    	StackPane pane = loader.load();
  	    	contentPane.getChildren().clear();
  	    	contentPane.getChildren().add(pane);
  	    }catch(IOException e) {
				e.printStackTrace();
			}

	    }
	  @FXML
	    void retour(ActionEvent event) {
		  FXMLLoader loader = new FXMLLoader(this.getClass().getResource("Eleve.fxml"));
  	    try {
  	    	StackPane pane = loader.load();
  	    	contentPane.getChildren().clear();
  	    	contentPane.getChildren().add(pane);
  	    }catch(IOException e) {
				e.printStackTrace();
			}

	    }
	
	public void calculSomme() {
		double somme =0.0;
		for(Note n: data) {
			somme+=n.getNote();
		}
		totalMatier.setText(data.size()+"");
		
	   totalNote.setText(somme+"");
	   
	   double moy = somme/data.size();
	   moyenne.setText(moy+"");
	}

	public void initialize(URL location, ResourceBundle resources) {
		matiereCol.setCellValueFactory(new PropertyValueFactory<Note, String>("matiere"));
		noteCol.setCellValueFactory(new PropertyValueFactory<Note, Double>("note"));
		appreciCol.setCellValueFactory(new PropertyValueFactory<Note, String>("mention"));
		
		nomLabel.setText(EleveController.selectedEleve.getNom() + "  " + EleveController.selectedEleve.getPrenom());
		nomLabel.setStyle("-fx-font:bold");
		afficher();
		calculSomme();
	}
	
	public void afficher() {
		data.clear();
		try {
			Connection con = ConnexionBD.getConnexion();
			String sql = "select * from note where idEl=? ";
			PreparedStatement stm = con.prepareStatement(sql);
			stm.setInt(1, EleveController.selectedEleve.getId());
			ResultSet rs = stm.executeQuery();

			while (rs.next()) {
				data.add(new Note(rs.getString(2), rs.getDouble(3),rs.getString(4)) );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		noteTable.setItems(data);

}
}