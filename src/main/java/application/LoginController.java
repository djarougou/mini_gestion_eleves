package application;

import java.io.IOException;

import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class LoginController {
	   @FXML
	    private JFXTextField txtUser;

	    @FXML
	    private JFXPasswordField txtPass;
	    @FXML
	    private StackPane contentPane;

	    @FXML
	    void charGerPage(ActionEvent event) {
	    	String user = txtUser.getText();
	    	String pass = txtPass.getText();
	    	if(user.equals("admin") && pass.equals("admin")) {
	    		
	    		FXMLLoader loader = new FXMLLoader(this.getClass().getResource("Eleve.fxml"));
	    	    try {
	    	    	StackPane pane = loader.load();
	    	    	contentPane.getChildren().clear();
	    	    	contentPane.getChildren().add(pane);
	    	    }catch(IOException e) {
					e.printStackTrace();
				}
				/*
				 * Stage primaryStage = new Stage(); try { Parent root =
				 * FXMLLoader.load(getClass().getResource("Eleve.fxml")); Scene scene = new
				 * Scene(root);
				 * //scene.getStylesheets().add(getClass().getResource("application.css").
				 * toExternalForm()); primaryStage.setScene(scene); primaryStage.show(); }
				 * catch(Exception e) { e.printStackTrace(); }
				 */
	    	}

	    }
}
