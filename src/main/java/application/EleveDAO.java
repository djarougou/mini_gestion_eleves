package application;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class EleveDAO {
	
	public static ObservableList<Eleve>  listEleves() {
		ObservableList<Eleve> list = FXCollections.observableArrayList();
		Eleve e0 = new Eleve(1, "ALUI", "jean", "djar@gmail.com");
		Eleve e1 = new Eleve(2, "TODO", "julien","abcd@gmail.com");
		Eleve e2 = new Eleve(3, "TATA", "pierrette", "talar@yahoo.fr");
		Eleve e3 = new Eleve(4, "MOLUH", "Hassan", "torienpd@gmail.com");
		Eleve e4 = new Eleve(5, "DOUDOU", "Asmaou", "samou@gmail.com");
		Eleve e5 = new Eleve(6, "DADA", "jeannette", "1jean@gmail.com");
		Eleve e6 = new Eleve(7, "TIMON", "Marcel", "marceld@gmail.com");
		Eleve e7 = new Eleve(8, "FADI", "Chamsia", "tchamed@gmail.com");
		Eleve e8 = new Eleve(9, "TOUMAI", "Ibrahim", "ibara@gmail.com");
		Eleve e9 = new Eleve(10, "MATUDU", "Bin", "mabi@gmail.com");
		
		Eleve e10 = new Eleve(11, "VALA", "Rosnie", "rosenv@gmail.com");
		Eleve e11 = new Eleve(12, "TOUKOUR", "Aissatou", "aiss@gmail.com");
		Eleve e12 = new Eleve(13, "BARKATOU", "Koubra", "koub@gmail.com");
		 

		list.addAll(e0,e1,e2,e3,e4,e5,e6,e7,e8,e9,e10,e11,e12);
		return list;
	 
	}
}
