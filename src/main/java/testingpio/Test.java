package testingpio;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class Test {
	public static void main(String[] args) throws FileNotFoundException, IOException {
		HSSFWorkbook exel = new HSSFWorkbook();
		exel.createSheet("hello");
		exel.createSheet("toto");
		
		exel.write(new FileOutputStream("teste.xls"));
	}

}
